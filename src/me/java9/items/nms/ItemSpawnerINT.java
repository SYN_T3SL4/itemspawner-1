package me.java9.items.nms;

import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface ItemSpawnerINT {

    public void setItemSpawner(Player p, ItemSpawnerType ist, Location loc);

}
