package me.java9.items.nms.spawnerversions;

import me.java9.items.api.API;
import me.java9.items.nms.ItemSpawnerINT;
import me.java9.items.serialize.ItemSpawnerType;
import net.minecraft.server.v1_12_R1.*;
import net.minecraft.server.v1_12_R1.TileEntityMobSpawner;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Player;

public class ItemSpawner_1_12_R1 implements ItemSpawnerINT {

    @Override
    public void setItemSpawner(Player p, ItemSpawnerType ist, Location loc) {
        BlockPosition blockPos = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        TileEntityMobSpawner spawner = (TileEntityMobSpawner) ((CraftWorld)loc.getWorld()).getHandle().getTileEntity(blockPos);
        NBTTagCompound nbtTileEntity = spawner.d();

        org.bukkit.inventory.ItemStack items = p.getInventory().getItemInMainHand();
        p.getInventory().setItemInMainHand(ist.getNecikaracagi().getItemStack());
        p.updateInventory();
        org.bukkit.inventory.ItemStack items2 = p.getItemInHand();

        NBTTagCompound itemstack = new NBTTagCompound();

        NBTTagCompound itemStackTag = new NBTTagCompound();
        ItemStack itemStack = CraftItemStack.asNMSCopy(items2);
        itemStack.save(itemStackTag);

        itemstack.setString("id","Item");
        itemstack.set("Item", itemStackTag);

        nbtTileEntity.set("SpawnData",itemstack);
        nbtTileEntity.setShort("SpawnCount", (short)ist.getCount());
        nbtTileEntity.setInt("MinSpawnDelay", ist.getMindelay()*20);
        nbtTileEntity.setInt("MaxSpawnDelay", ist.getMaxdelay()*20);
        nbtTileEntity.setShort("SpawnRange",(short)ist.getRange());
        nbtTileEntity.setShort("RequiredPlayerRange", (short)ist.getRequiredrange());

        spawner.a(nbtTileEntity);

        p.getInventory().setItemInMainHand(items);
        p.updateInventory();
    }

}
