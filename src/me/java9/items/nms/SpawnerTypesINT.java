package me.java9.items.nms;

import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Location;

public interface SpawnerTypesINT {

    public ItemSpawnerType getItemSpawnerTypeFromLocation(Location loc);

}
