package me.java9.items.nms.spawnertypes;

import me.java9.items.api.API;
import me.java9.items.api.Styles;
import me.java9.items.nms.SpawnerTypesINT;
import me.java9.items.serialize.ItemSpawnerType;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemFactory;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;

import java.util.Objects;

public class SpawnerTypes_1_12_R1 implements SpawnerTypesINT {

    @Override
    public ItemSpawnerType getItemSpawnerTypeFromLocation(Location loc) {
        if (loc.getBlock().getType() == Material.MOB_SPAWNER) {
            BlockPosition blockPos = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
            TileEntityMobSpawner spawner = (TileEntityMobSpawner) ((CraftWorld)loc.getWorld()).getHandle().getTileEntity(blockPos);
            NBTTagCompound nbtTileEntity = spawner.d();

            NBTTagCompound itemStacknbt_1 = (NBTTagCompound) nbtTileEntity.get("SpawnData");
            NBTTagCompound itemStacknbt_2 = (NBTTagCompound) itemStacknbt_1.get("Item");

            for (ItemSpawnerType ist : Styles.ItemSpawner_Types) {
                ItemStack itemStack = CraftItemStack.asNMSCopy(ist.getNecikaracagi().getItemStack());
                NBTTagCompound itemStack_tmp = new NBTTagCompound();
                itemStack.save(itemStack_tmp);
                if (Objects.equals(itemStack_tmp.toString(),itemStacknbt_2.toString())) {
                    return ist;
                }
            }
        }
        return null;
    }
}
