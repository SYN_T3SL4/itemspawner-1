package me.java9.items.api;

import me.java9.items.ItemSpawnerPlugin;

import java.util.Objects;

public class Dil {

    public Dil(ItemSpawnerPlugin plugin, String language) {
        if (Objects.equals(language,"en")) {
            API.ayarlar = new ConfigAPI(plugin, "options", "en");
            API.komutlar = new ConfigAPI(plugin, "commands", "en");
            API.spawnerlar = new ConfigAPI(plugin, "spawners", "en");
        } else if (Objects.equals(language,"tr")) {
            API.ayarlar = new ConfigAPI(plugin, "ayarlar", "tr");
            API.komutlar = new ConfigAPI(plugin, "komutlar", "tr");
            API.spawnerlar = new ConfigAPI(plugin, "spawnerlar", "tr");
        }
    }

}
