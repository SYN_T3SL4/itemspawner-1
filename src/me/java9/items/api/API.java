package me.java9.items.api;

import me.java9.items.ItemSpawnerPlugin;
import me.java9.items.commands.CommandHandler;
import me.java9.items.commands.itemsp.general.ItemSpawnerReloadCommand;
import me.java9.items.commands.itemsp.spawner.*;
import me.java9.items.listeners.update.ItemSpawnerUpdateListener;
import me.java9.items.nms.ItemSpawnerINT;
import me.java9.items.nms.SpawnerTypesINT;
import me.java9.items.serialize.CustomItem;
import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class API {

    public static String Plugin_Language = "en";

    public static ConfigAPI languages;
    public static ConfigAPI spawnerlar;
    public static ConfigAPI komutlar;
    public static ConfigAPI ayarlar;

    public static String version;
    public static ItemSpawnerINT item;
    public static SpawnerTypesINT spawnertypes;

    public static String chatcolor(String s) {
        return ChatColor.translateAlternateColorCodes('&',s);
    }

    public static List<String> getChatColorList(List<String> list) {
        List<String> liste = new ArrayList<>();
        list.stream().forEach(templist -> liste.add(API.chatcolor(templist)));
        return liste;
    }

    public static void registerListener() {
        registerListeners(new ItemSpawnerUpdateListener());
    }

    private static void registerListeners(Listener... listeners){
        Arrays.stream(listeners).forEach(listener -> Bukkit.getPluginManager().registerEvents(listener,ItemSpawnerPlugin.getPlugin()));
    }

    public static void registerCommands() {
        CommandHandler handler = new CommandHandler(Styles.Main_Command);
        handler.addArgs(Styles.Reload_Arg,new ItemSpawnerReloadCommand());
        handler.addArgs(Styles.List_Arg,new ItemSpawnerListeleCommand());
        handler.addArgs(Styles.Give_Arg,new ItemSpawnerVerCommand());
        handler.addArgs(Styles.Deal_Arg,new ItemSpawnerDagitCommand());
        handler.build();
    }

    public static boolean checkInventory(Player p, ItemStack item) {
        if (p.getInventory().firstEmpty() >= 0 && item.getAmount() <= item.getMaxStackSize()) {
            return true;
        }
        Map<Integer, ? extends ItemStack> items = p.getInventory().all(item.getType());
        int amount = item.getAmount();
        for (ItemStack i : items.values()) {
            amount -= i.getMaxStackSize() - i.getAmount();
        }
        if (amount <= 0) {
            return p.getInventory().firstEmpty() != -1;
        }
        return false;
    }

    public static ItemSpawnerType getItemSpawnerTypeFromItemStack(ItemStack itemInHand) {
        for (ItemSpawnerType ist : Styles.ItemSpawner_Types) {
            if (itemInHand.isSimilar(ist.getKendisi().getItemStack())) {
                return ist;
            }
        }
        return null;
    }

    public static ItemSpawnerType getItemSpawnerTypeFromID(String arg) {
        for (ItemSpawnerType ist : Styles.ItemSpawner_Types) {
            if (Objects.equals(ist.getSpawnerid(),arg)) {
                return ist;
            }
        }
        return null;
    }

    public static void sendUpdateNotif(Updater updater,CommandSender sender) {
        sender.sendMessage(API.chatcolor("&6====================&e&l[ItemSpawner]&6===================="));
        sender.sendMessage(API.chatcolor("&eAn update is available"));
        sender.sendMessage(API.chatcolor("&eCurrent version is &c" + ItemSpawnerPlugin.getPlugin().getDescription().getVersion()));
        sender.sendMessage(API.chatcolor("&eNew version is &a" + updater.getLatestVersion()));
        sender.sendMessage(API.chatcolor("&7" + updater.getResourceURL()));
        sender.sendMessage(API.chatcolor("&6====================&e&l[ItemSpawner]&6===================="));
    }
}
