package me.java9.items.api;

import me.java9.items.ItemSpawnerPlugin;
import me.java9.items.serialize.CustomItem;
import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Styles {

    //Commands and Args
    static String Main_Command;
    static String Reload_Arg;
    static String Deal_Arg;
    static String Give_Arg;
    static String List_Arg;
    //Commands and Args

    //Information Messages
    public static String Plugin_Prefix;
    public static String Help_Prefix;
    public static String Help_Reload;
    public static String Help_Deal;
    public static String Help_Give;
    public static String Help_List;
    public static String Spawner_List;
    //Information Messages

    //General Options
    public static int SilkTouch_Level_For_Break_To_Spawner;
    //General Options

    //General Messages
    public static String Files_Has_Been_Reloaded;
    public static String Spawner_Deal;
    public static String Spawner_Give;
    public static String Spawner_Has_Been_Gived;
    //General Messages

    //Error Messages
    public static String Player_Not_Found;
    public static String No_Permission_Error;
    public static String Wrong_ID;
    public static String Inventory_Is_Full;
    public static String Player_Inventory_Is_Full;
    public static String Insufficient_SilkTouch_Level;
    public static String Just_Breakable_With_Pickaxe;
    static String Unsupporting_Version;
    //Error Messages

    //Temp
    private static String languge;
    private static ConfigAPI komutlar;
    private static ConfigAPI ayarlar;
    private static ConfigAPI spawnerlar;

    public static List<ItemSpawnerType> ItemSpawner_Types = new ArrayList<>();
    //Temp

    public Styles() {
        komutlar = API.komutlar;
        ayarlar = API.ayarlar;
        languge = API.Plugin_Language;
        spawnerlar = API.spawnerlar;
        preFiles();
    }

    private static void preFiles() {
        if (Objects.equals(languge, "en")) {

            preOptions("Main_Command",
                    "Args.Deal_Arg",
                    "Args.Give_Arg",
                    "Help_Prefix",
                    "Help_Reload",
                    "Help_Deal",
                    "Help_Give",
                    "Help_List",
                    "Spawner_List",
                    "SilkTouch_Level_For_Break_To_Spawner",
                    "Files_Has_Been_Reloaded",
                    "Spawner_Deal",
                    "Spawner_Give",
                    "Spawner_Has_Been_Gived",
                    "Player_Not_Found",
                    "No_Permission_Error",
                    "Wrong_ID",
                    "Inventory_Is_Full",
                    "Player_Inventory_Is_Full",
                    "Insufficient_SilkTouch_Level",
                    "Just_Breakable_With_Pickaxe",
                    "Unsupporting_Version");

            //Spawners
            for (String s : spawnerlar.getConfigurationSection("Spawner_Types").getKeys(false)) {
                String spawner_id = spawnerlar.getConfig().getString("Spawner_Types." + s + ".Spawner_ID");
                String spawner_name = spawnerlar.getConfig().getString("Spawner_Types." + s + ".Spawner_Name");
                List<String> spawner_lore = spawnerlar.getConfig().getStringList("Spawner_Types." + s + ".Spawner_Lore");
                CustomItem spawner_customitem = new CustomItem(spawner_id, spawner_name, spawner_lore);

                int spawner_spawncount = spawnerlar.getConfig().getInt("Spawner_Types." + s + ".Spawner_SpawnCount");
                int spawner_spawnrange = spawnerlar.getConfig().getInt("Spawner_Types." + s + ".Spawner_SpawnRange");
                int spawner_minspawndelay = spawnerlar.getConfig().getInt("Spawner_Types." + s + ".Spawner_MinSpawnerDelay");
                int spawner_maxspawndelay = spawnerlar.getConfig().getInt("Spawner_Types." + s + ".Spawner_MaxSpawnerDelay");
                int spawner_requiredplayerrange = spawnerlar.getConfig().getInt("Spawner_Types." + s + ".Spawner_RequiredPlayerRange");

                String dropped_item_id = spawnerlar.getConfig().getString("Spawner_Types." + s + ".Dropped_Item_ID");
                String dropped_item_name = spawnerlar.getConfig().getString("Spawner_Types." + s + ".Dropped_Item_Name");
                List<String> dropped_item_lore = spawnerlar.getConfig().getStringList("Spawner_Types." + s + ".Dropped_Item_Lore");
                CustomItem dropped_item_customitem = new CustomItem(dropped_item_id, dropped_item_name, dropped_item_lore);

                ItemSpawner_Types.add(new ItemSpawnerType(s,spawner_customitem,dropped_item_customitem,
                        spawner_spawncount,spawner_spawnrange,spawner_minspawndelay,spawner_maxspawndelay,
                        spawner_requiredplayerrange));
            }
            //Spawners

        } else if (Objects.equals(languge, "tr")) {

            preOptions("Ana_Komut",
                    "Args.Dagit_Arg",
                    "Args.Ver_Arg",
                    "Yardim_Prefix",
                    "Yardim_Reload",
                    "Yardim_Dagit",
                    "Yardim_Ver",
                    "Yardim_List",
                    "Spawner_List",
                    "ItemSpawner_Kirmak_Icin_Gerekli_SilkTouch_Seviyesi",
                    "Dosyalar_Yeniden_Yuklendi",
                    "Spawner_Dagit",
                    "Spawner_Ver",
                    "Spawner_Verildi",
                    "Oyuncu_Bulunamadi",
                    "Yetki_Hatasi",
                    "Yanlis_ID",
                    "Envanter_Dolu",
                    "Oyuncunun_Envanteri_Dolu",
                    "Yetersiz_SilkTouch_Seviyesi",
                    "Yalnizca_Kazma_Ile_Kilir",
                    "Desteklenmeyen_Surum");

            //Spawnerlar
            for (String s : spawnerlar.getConfigurationSection("Spawner_Tipleri").getKeys(false)) {
                String spawner_id = spawnerlar.getConfig().getString("Spawner_Tipleri." + s + ".Spawner_ID");
                String spawner_name = API.chatcolor(spawnerlar.getConfig().getString("Spawner_Tipleri." + s + ".Spawner_Ismi"));
                List<String> spawner_lore = API.getChatColorList(spawnerlar.getConfig().getStringList("Spawner_Tipleri." + s + ".Spawner_Aciklamasi"));
                CustomItem spawner_customitem = new CustomItem(spawner_id,spawner_name,spawner_lore);

                int spawner_spawncount = spawnerlar.getConfig().getInt("Spawner_Tipleri." + s + ".Spawner_CikacakMiktar");
                int spawner_spawnrange = spawnerlar.getConfig().getInt("Spawner_Tipleri." + s + ".Spawner_CikacakMesafe");
                int spawner_minspawndelay = spawnerlar.getConfig().getInt("Spawner_Tipleri." + s + ".Spawner_MinHizi");
                int spawner_maxspawndelay = spawnerlar.getConfig().getInt("Spawner_Tipleri." + s + ".Spawner_MaxHizi");
                int spawner_requiredplayerrange = spawnerlar.getConfig().getInt("Spawner_Tipleri." + s + ".Spawner_CalismaMesafesi");

                String dropped_item_id = spawnerlar.getConfig().getString("Spawner_Tipleri." + s + ".Dusen_Esya_ID");
                String dropped_item_name = API.chatcolor(spawnerlar.getConfig().getString("Spawner_Tipleri." + s + ".Dusen_Esya_Ismi"));
                List<String> dropped_item_lore = API.getChatColorList(spawnerlar.getConfig().getStringList("Spawner_Tipleri." + s + ".Dusen_Esya_Aciklamasi"));
                CustomItem dropped_item_customitem = new CustomItem(dropped_item_id,dropped_item_name,dropped_item_lore);

                ItemSpawner_Types.add(new ItemSpawnerType(s,spawner_customitem,dropped_item_customitem,
                        spawner_spawncount,spawner_spawnrange,spawner_minspawndelay,spawner_maxspawndelay,
                        spawner_requiredplayerrange));
            }
            //Spawnerlar
        }
    }

    private static String setArgs(String s) {
        for (String ss : komutlar.getConfigurationSection("Args").getKeys(false)) {
            String sss = komutlar.getConfig().getString("Args." + ss);
            s = s.replaceAll("%" + ss + "%", sss);
        }
        if (Objects.equals(languge, "en")) {
            return s.replaceAll("%Main_Command%", Main_Command);
        } else if (Objects.equals(languge, "tr")) {
            return s.replaceAll("%Ana_Komut%", Main_Command);
        }
        Bukkit.getConsoleSender().sendMessage(API.chatcolor("&e[ItemSpawner] &cUnsupporting language, plugin can not be loaded!"));
        ItemSpawnerPlugin.getPlugin().getServer().getPluginManager().disablePlugin(ItemSpawnerPlugin.getPlugin());
        return "";
    }

    private static String setPrefix(String s) {
        return s.replaceAll("%Prefix%", Plugin_Prefix);
    }

    private static void preOptions(String main_Command, String deal_Arg, String give_Arg, String help_Prefix, String help_Reload,
                                   String help_Deal, String help_Give, String help_List, String spawner_List,
                                   String silkTouch_Level_For_Break_To_Spawner, String files_Has_Been_Reloaded,
                                   String spawner_Deal, String spawner_Give, String spawner_Has_Been_Gived,
                                   String player_Not_Found, String no_Permission_Error, String wrong_ID,
                                   String inventory_Is_Full, String player_Inventory_Is_Full,
                                   String insufficient_SilkTouch_Level, String just_Breakable_With_Pickaxe,
                                   String unsupporting_Version) {
        //Commands and Args
        Main_Command = komutlar.getConfig().getString(main_Command);
        Reload_Arg = komutlar.getConfig().getString("Args.Reload_Arg");
        Deal_Arg = komutlar.getConfig().getString(deal_Arg);
        Give_Arg = komutlar.getConfig().getString(give_Arg);
        List_Arg = komutlar.getConfig().getString("Args.List_Arg");
        //Commands and Args

        //Information Messages
        Plugin_Prefix = API.chatcolor(ayarlar.getConfig().getString("Plugin_Prefix"));
        Help_Prefix = API.chatcolor(setArgs(ayarlar.getConfig().getString(help_Prefix)));
        Help_Reload = API.chatcolor(setArgs(ayarlar.getConfig().getString(help_Reload)));
        Help_Deal = API.chatcolor(setArgs(ayarlar.getConfig().getString(help_Deal)));
        Help_Give = API.chatcolor(setArgs(ayarlar.getConfig().getString(help_Give)));
        Help_List = API.chatcolor(setArgs(ayarlar.getConfig().getString(help_List)));
        Spawner_List = API.chatcolor(setArgs(ayarlar.getConfig().getString(spawner_List)));
        //Information Messages

        //General Options
        SilkTouch_Level_For_Break_To_Spawner = ayarlar.getConfig().getInt(silkTouch_Level_For_Break_To_Spawner);
        //General Options

        //General Message
        Files_Has_Been_Reloaded = API.chatcolor(setPrefix(ayarlar.getConfig().getString(files_Has_Been_Reloaded)));
        Spawner_Deal = API.chatcolor(setPrefix(ayarlar.getConfig().getString(spawner_Deal)));
        Spawner_Give = API.chatcolor(setPrefix(ayarlar.getConfig().getString(spawner_Give)));
        Spawner_Has_Been_Gived = API.chatcolor(setPrefix(ayarlar.getConfig().getString(spawner_Has_Been_Gived)));
        //General Message

        //Error Messages
        Player_Not_Found = API.chatcolor(setPrefix(ayarlar.getConfig().getString(player_Not_Found)));
        No_Permission_Error = API.chatcolor(setPrefix(ayarlar.getConfig().getString(no_Permission_Error)));
        Wrong_ID = API.chatcolor(setPrefix(ayarlar.getConfig().getString(wrong_ID)));
        Inventory_Is_Full = API.chatcolor(setPrefix(ayarlar.getConfig().getString(inventory_Is_Full)));
        Player_Inventory_Is_Full = API.chatcolor(setPrefix(ayarlar.getConfig().getString(player_Inventory_Is_Full)));
        Insufficient_SilkTouch_Level = API.chatcolor(setPrefix(ayarlar.getConfig().getString(insufficient_SilkTouch_Level)));
        Just_Breakable_With_Pickaxe = API.chatcolor(setPrefix(ayarlar.getConfig().getString(just_Breakable_With_Pickaxe)));
        Unsupporting_Version = API.chatcolor(setPrefix(ayarlar.getConfig().getString(unsupporting_Version)));
        //Error Messages
    }
}