package me.java9.items.api;

import me.java9.items.ItemSpawnerPlugin;
import me.java9.items.nms.*;
import me.java9.items.nms.spawnertypes.SpawnerTypes_1_12_R1;
import me.java9.items.nms.spawnerversions.*;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.Objects;

public class Initializing implements Listener{

    private static ItemSpawnerPlugin plugin;

    public static void startPlugin(ItemSpawnerPlugin instance) {
        plugin = instance;

        plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
            public void run() {
                Updater updater = new Updater(plugin,44354);
                try {
                    if (updater.checkForUpdates()) {
                        API.sendUpdateNotif(updater,Bukkit.getConsoleSender());
                    }
                } catch (Exception e) {
                    plugin.getLogger().warning("Could not check for updates! Stacktrace:");
                    e.printStackTrace();
                }
            }
        }, 20L);

        API.languages = new ConfigAPI(plugin,"languages");

        if (API.languages.getConfig().get("Plugin_Language") != null) {
            API.Plugin_Language = API.languages.getConfig().getString("Plugin_Language");
            if (Objects.equals(API.Plugin_Language,"en") || Objects.equals(API.Plugin_Language,"tr")) {
                new Dil(plugin,API.Plugin_Language);
            } else {
                Bukkit.getConsoleSender().sendMessage(API.chatcolor("&e[ItemSpawner] &cUnsupporting language, plugin can not be loaded!"));
                plugin.getServer().getPluginManager().disablePlugin(plugin);
                return;
            }
        }

        String a = plugin.getServer().getClass().getPackage().getName();
        API.version = a.substring(a.lastIndexOf('.') + 1);

        new Styles();

        if (setupItemSpawner(API.version) == null) {
            Bukkit.getConsoleSender().sendMessage(Styles.Unsupporting_Version);
            plugin.getServer().getPluginManager().disablePlugin(plugin);
            return;
        }

        API.item = setupItemSpawner(API.version);

        API.registerCommands();
        API.registerListener();
    }

    public static void stopPlugin(ItemSpawnerPlugin instance) {
        plugin = instance;

    }

    private static ItemSpawnerINT setupItemSpawner(String version){
        /**if (Objects.equals(version, "v1_8_R1")) {
            return new ItemSpawner_1_8_R1();
        } else if (Objects.equals(version,"v1_8_R2")) {
            return new ItemSpawner_1_8_R2();
        } else if (Objects.equals(version, "v1_8_R3")) {
            return new ItemSpawner_1_8_R3();
        } else if (Objects.equals(version, "v1_9_R1")) {
            return new ItemSpawner_1_9_R1();
        } else if (Objects.equals(version, "v1_9_R2")) {
            return new ItemSpawner_1_9_R2();
        } else if (Objects.equals(version, "v1_10_R1")) {
            return new ItemSpawner_1_10_R1();
        } else if (Objects.equals(version, "v1_11_R1")) {
            return new ItemSpawner_1_11_R1();
        } else**/ if (Objects.equals(version, "v1_12_R1")) {
            API.spawnertypes = new SpawnerTypes_1_12_R1();
            return new ItemSpawner_1_12_R1();
        }
        return null;
    }

}
