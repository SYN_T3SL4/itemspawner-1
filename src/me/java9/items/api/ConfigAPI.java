package me.java9.items.api;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

public class ConfigAPI {

    private File configFile;
    private String filename;
    private JavaPlugin plugin;
    private FileConfiguration fileConfiguration;

    public ConfigAPI(JavaPlugin plugin, String filename) {
        this.filename = filename + ".yml";
        this.plugin = plugin;
        configFile = new File(plugin.getDataFolder(), this.filename);
        firstRun(plugin);
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
        load();
    }

    public ConfigAPI(JavaPlugin plugin, String filename,String language) {
        this.filename = filename + ".yml";
        this.plugin = plugin;
        configFile = new File(plugin.getDataFolder()+"/"+language, this.filename);
        firstRun(plugin,language);
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
        load();
    }

    public FileConfiguration getConfig() {
        load();
        return fileConfiguration;
    }

    public void save() {
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            fileConfiguration.load(configFile);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            configFile = new File(plugin.getDataFolder(), filename);
            firstRun(plugin);
            fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void firstRun(JavaPlugin plugin,String language) {
        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            copy(plugin.getResource(language+"/"+filename), configFile);
        }
    }

    private void firstRun(JavaPlugin plugin) {
        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            copy(plugin.getResource(filename), configFile);
        }
    }

    private void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[63];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception localException) {}
    }

    public ConfigurationSection getConfigurationSection(String path) {
        return getConfig().getConfigurationSection(path);
    }
}
