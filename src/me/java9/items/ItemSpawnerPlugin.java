package me.java9.items;

import me.java9.items.api.Initializing;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemSpawnerPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        Initializing.startPlugin(this);
    }

    @Override
    public void onDisable() {
        Initializing.stopPlugin(this);
    }

    public static ItemSpawnerPlugin getPlugin(){
        return (ItemSpawnerPlugin) Bukkit.getPluginManager().getPlugin("ItemSpawner");
    }
}
