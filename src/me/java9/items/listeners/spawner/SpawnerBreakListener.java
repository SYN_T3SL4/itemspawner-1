package me.java9.items.listeners.spawner;

import me.java9.items.api.API;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class SpawnerBreakListener implements Listener{

    @EventHandler
    public void spawnerBreakListener(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.MOB_SPAWNER) {
            Player p = e.getPlayer();
            if (API.spawnertypes.getItemSpawnerTypeFromLocation(
                    e.getBlock().getLocation()).getKendisi().getItemStack() != null) {
                e.setCancelled(true);
                e.getBlock().breakNaturally();
                e.getBlock().getDrops().clear();
                ItemStack itemStack = API.spawnertypes.getItemSpawnerTypeFromLocation(
                        e.getBlock().getLocation()).getKendisi().getItemStack();
                if (API.checkInventory(p, itemStack)) {
                    p.getInventory().addItem(itemStack);
                }
            }
        }
    }

}
