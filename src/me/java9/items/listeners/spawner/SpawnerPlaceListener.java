package me.java9.items.listeners.spawner;

import me.java9.items.ItemSpawnerPlugin;
import me.java9.items.api.API;
import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class SpawnerPlaceListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void a(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        if (API.getItemSpawnerTypeFromItemStack(p.getItemInHand()) != null) {
            ItemSpawnerType ist = API.getItemSpawnerTypeFromItemStack(p.getItemInHand());
            if (!e.isCancelled()) {
                e.setCancelled(true);
                Bukkit.getScheduler().runTaskLater(ItemSpawnerPlugin.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        p.getInventory().removeItem(e.getItemInHand());
                        p.updateInventory();
                        API.item.setItemSpawner(p,ist,e.getBlock().getLocation());
                    }
                }, 1);
            }

        }
    }

}
