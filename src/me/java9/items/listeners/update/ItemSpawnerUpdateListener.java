package me.java9.items.listeners.update;

import me.java9.items.ItemSpawnerPlugin;
import me.java9.items.api.API;
import me.java9.items.api.Updater;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ItemSpawnerUpdateListener implements Listener {

    @EventHandler (priority = EventPriority.NORMAL)
    public void a(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (p.isOp()) {
            Updater updater = new Updater(ItemSpawnerPlugin.getPlugin(),44354);
            try {
                if (updater.checkForUpdates()) {
                    API.sendUpdateNotif(updater,p);
                }
            } catch (Exception ex) {
                Bukkit.getLogger().warning("Could not check for updates! Stacktrace:");
                ex.printStackTrace();
            }
        }
    }
}
