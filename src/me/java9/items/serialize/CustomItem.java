package me.java9.items.serialize;

import me.java9.items.api.API;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CustomItem {

    private int id;
    private byte data;
    private int amount;
    private String name;
    private List<String> lore = new ArrayList<String>();

    public CustomItem() {
    }

    public CustomItem(ItemStack item) {
        id = item.getTypeId();
        data = item.getData().getData();
        amount = item.getAmount();
        name = item.getItemMeta().getDisplayName();
        lore = item.getItemMeta().getLore();
    }

    public CustomItem(int id, byte data, int amount, String name, List<String> lore) {
        this.id = id;
        this.data = data;
        this.amount = amount;
        this.name = name;
        this.lore = lore;
    }

    public CustomItem(int id, int data, String name, List<String> lore) {
        this.id = id;
        this.data = (byte)data;
        amount = 1;
        this.name = name;
        this.lore = lore;
    }

    public CustomItem(String id, String name, List<String> lore) {
        String[] tmp_id = id.split(":");
        if (tmp_id.length == 1) {
            this.id = Integer.parseInt(tmp_id[0]);
            data = (byte)0;
        }
        if (tmp_id.length == 2) {
            this.id = Integer.parseInt(tmp_id[0]);
            data = Byte.parseByte(tmp_id[1]);
        }
        amount = 1;
        this.name = name;
        this.lore = lore;
    }

    public void set(ItemStack itemstack) {
        id = itemstack.getTypeId();
        data = itemstack.getData().getData();
        amount = itemstack.getAmount();
        if (itemstack.hasItemMeta()) {
            ItemMeta meta = itemstack.getItemMeta();
            if (meta.hasDisplayName()) {
                name = itemstack.getItemMeta().getDisplayName();
            }
            if (meta.hasLore()) {
                lore = itemstack.getItemMeta().getLore();
            }
        }
    }

    public ItemStack getItemStack() {
        if (id != 0 && Material.getMaterial(id) != null) {
            ItemStack item;
            if (amount == 0) {
                item = new ItemStack(Material.getMaterial(id), 1, (short) 1, data);
            } else {
                item = new ItemStack(Material.getMaterial(id), amount, (short) 1, data);
            }
            ItemMeta m = item.getItemMeta();
            m.setDisplayName(name);
            m.setLore(lore);
            item.setItemMeta(m);
            return item;
        }
        return null;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLore() {
        return lore;
    }

    public void setLore(List<String> lore) {
        this.lore = lore;
    }
}
