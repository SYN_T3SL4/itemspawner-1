package me.java9.items.serialize;

import me.java9.items.ItemSpawnerPlugin;
import me.java9.items.api.API;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.Serializable;
import java.util.Objects;

public class ItemSpawnerType {

    private String spawnerid;
    private CustomItem necikaracagi;
    private CustomItem kendisi;
    private int count;
    private int range;
    private int mindelay;
    private int maxdelay;
    private int requiredrange;

    public ItemSpawnerType(String spawner_id, CustomItem spawner_customitem, CustomItem dropped_item_customitem,
                           int spawner_count, int spawner_range, int spawner_mindelay, int spawner_maxdelay,
                           int spawner_requiredrange) {
        spawnerid = spawner_id;
        necikaracagi = dropped_item_customitem;
        kendisi = spawner_customitem;
        count = spawner_count;
        range = spawner_range;
        mindelay = spawner_mindelay;
        maxdelay = spawner_maxdelay;
        requiredrange = spawner_requiredrange;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getMindelay() {
        return mindelay;
    }

    public void setMindelay(int mindelay) {
        this.mindelay = mindelay;
    }

    public int getMaxdelay() {
        return maxdelay;
    }

    public void setMaxdelay(int maxdelay) {
        this.maxdelay = maxdelay;
    }

    public int getRequiredrange() {
        return requiredrange;
    }

    public void setRequiredrange(int requiredrange) {
        this.requiredrange = requiredrange;
    }

    public String getSpawnerid() {
        return spawnerid;
    }

    public void setSpawnerid(String spawnerid) {
        this.spawnerid = spawnerid;
    }

    public CustomItem getNecikaracagi() {
        return necikaracagi;
    }

    public void setNecikaracagi(CustomItem necikaracagi) {
        this.necikaracagi = necikaracagi;
    }

    public CustomItem getKendisi() {
        return kendisi;
    }

    public void setKendisi(CustomItem kendisi) {
        this.kendisi = kendisi;
    }
}
