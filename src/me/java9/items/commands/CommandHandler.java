package me.java9.items.commands;

import me.java9.items.ItemSpawnerPlugin;

import me.java9.items.api.Styles;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;

public class CommandHandler implements CommandExecutor {

    private HashMap<String, CommandInterface> args = new HashMap<String, CommandInterface>();
    private String commandLabel;
    StringBuilder helpmessage = a();

    public CommandHandler(String commandLabel) {
        this.commandLabel = commandLabel;
    }

    public void addArgs(String args, CommandInterface arg) {
        this.args.put(args,arg);
    }

    public boolean exists(String args) {
        return this.args.containsKey(args);
    }

    public CommandInterface getExecutor(String args) {
        return this.args.get(args);
    }

    public void build() {
        ItemSpawnerPlugin.getPlugin().getCommand(commandLabel).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(helpmessage.toString());
        }
        if (args.length > 0) {
            if (exists(args[0])) {
                getExecutor(args[0]).onCommand(sender, cmd, commandLabel, args);
                return true;
            } else {
                sender.sendMessage(helpmessage.toString());
                return false;
            }
        }
        return false;
    }

    private StringBuilder a() {
        helpmessage = new StringBuilder();
        helpmessage.append(Styles.Help_Prefix+"\n");
        helpmessage.append(Styles.Help_Give+"\n");
        helpmessage.append(Styles.Help_Deal+"\n");
        helpmessage.append(Styles.Help_List+"\n");
        helpmessage.append(Styles.Help_Reload);
        return helpmessage;
    }

}
