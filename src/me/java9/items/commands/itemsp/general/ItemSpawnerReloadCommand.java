package me.java9.items.commands.itemsp.general;

import me.java9.items.api.API;
import me.java9.items.api.Styles;
import me.java9.items.commands.CommandInterface;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import javax.swing.text.Style;

public class ItemSpawnerReloadCommand implements CommandInterface {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender.isOp()) {
            API.ayarlar.load();
            API.spawnerlar.load();
            new Styles();
            sender.sendMessage(Styles.Files_Has_Been_Reloaded);
        } else {
            sender.sendMessage(Styles.No_Permission_Error);
        }
        return false;
    }
}
