package me.java9.items.commands.itemsp.spawner;

import me.java9.items.api.API;
import me.java9.items.api.Styles;
import me.java9.items.commands.CommandInterface;
import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ItemSpawnerVerCommand implements CommandInterface {

    private StringBuilder helpmessage = a();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (args.length == 3) {
            if (sender.isOp()) {
                if (Bukkit.getPlayer(args[1]) != null) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (API.getItemSpawnerTypeFromID(args[2]) != null) {
                        ItemSpawnerType type = API.getItemSpawnerTypeFromID(args[2]);
                        int amount = 1;
                        if (API.checkInventory(target, type.getKendisi().getItemStack())) {
                            target.getInventory().addItem(type.getKendisi().getItemStack());
                            target.updateInventory();
                            target.sendMessage(Styles.Spawner_Give);
                            sender.sendMessage(Styles.Spawner_Has_Been_Gived.replaceAll("%target%", target.getName()));
                        } else {
                            sender.sendMessage(Styles.Player_Inventory_Is_Full.replaceAll("%player%", target.getName()));
                        }
                    } else {
                        sender.sendMessage(Styles.Wrong_ID);
                    }
                } else {
                    sender.sendMessage(Styles.Player_Not_Found.replaceAll("%player%", args[1]));
                }
            } else {
                sender.sendMessage(Styles.No_Permission_Error);
            }
        } else if (args.length == 4) {
            if (sender.isOp()) {
                if (Bukkit.getPlayer(args[1]) != null) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (API.getItemSpawnerTypeFromID(args[2]) != null) {
                        ItemSpawnerType type = API.getItemSpawnerTypeFromID(args[2]);
                        int amount = 1;
                        try {
                            amount = Integer.parseInt(args[3]);
                        } catch (Exception ex) {
                            sender.sendMessage(helpmessage.toString());
                            return false;
                        }
                        if (API.checkInventory(target, type.getKendisi().getItemStack())) {
                            for (int i = 0; i < amount; i++) {
                                target.getInventory().addItem(type.getKendisi().getItemStack());
                                target.updateInventory();
                            }
                            target.sendMessage(Styles.Spawner_Give);
                        } else {
                            sender.sendMessage(Styles.Player_Inventory_Is_Full.replaceAll("%player%", target.getName()));
                        }
                    }
                } else {
                    sender.sendMessage(Styles.Player_Not_Found.replaceAll("%player%", args[1]));
                }
            } else {
                sender.sendMessage(Styles.No_Permission_Error);
            }
        } else {
            sender.sendMessage(helpmessage.toString());
        }
        return false;
    }

    private StringBuilder a() {
        helpmessage = new StringBuilder();
        helpmessage.append(Styles.Plugin_Prefix+"\n");
        helpmessage.append(Styles.Help_Give);
        return helpmessage;
    }

}
