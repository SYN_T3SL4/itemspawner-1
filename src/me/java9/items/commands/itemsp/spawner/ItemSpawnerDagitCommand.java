package me.java9.items.commands.itemsp.spawner;

import me.java9.items.api.API;
import me.java9.items.api.Styles;
import me.java9.items.commands.CommandInterface;
import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ItemSpawnerDagitCommand implements CommandInterface {

    private StringBuilder helpmessage = a();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player)sender;
            if (p.isOp()) {
                if (args.length == 2) {
                    if (API.getItemSpawnerTypeFromID(args[1]) != null) {
                        ItemSpawnerType type = API.getItemSpawnerTypeFromID(args[1]);
                        int amount = 1;
                        for (Player ps : Bukkit.getOnlinePlayers()) {
                            if (API.checkInventory(ps, type.getKendisi().getItemStack())) {
                                ps.getInventory().addItem(type.getKendisi().getItemStack());
                                ps.updateInventory();
                            } else {
                                p.sendMessage(Styles.Player_Inventory_Is_Full.replaceAll("%player%", ps.getName()));
                                ps.sendMessage(Styles.Inventory_Is_Full);
                            }
                        }
                    } else {
                        p.sendMessage(Styles.Wrong_ID);
                    }
                } else if (args.length == 3) {
                    if (API.getItemSpawnerTypeFromID(args[1]) != null) {
                        ItemSpawnerType type = API.getItemSpawnerTypeFromID(args[1]);
                        int amount = 1;
                        try {
                            amount = Integer.parseInt(args[2]);
                        } catch (Exception e) {
                            p.sendMessage(helpmessage.toString());
                            return false;
                        }
                        for (Player ps : Bukkit.getOnlinePlayers()) {
                            if (API.checkInventory(ps, type.getKendisi().getItemStack())) {
                                for (int i = 0; i < amount; i++) {
                                    ps.getInventory().addItem(type.getKendisi().getItemStack());
                                    ps.updateInventory();
                                }
                            } else {
                                p.sendMessage(Styles.Player_Inventory_Is_Full.replaceAll("%player%", ps.getName()));
                                ps.sendMessage(Styles.Inventory_Is_Full);
                            }
                        }
                    } else {
                        p.sendMessage(Styles.Wrong_ID);
                    }
                } else {
                    p.sendMessage(helpmessage.toString());
                }
            } else {
                p.sendMessage(Styles.No_Permission_Error);
            }
        } else {
            if (args.length == 2) {


                if (API.getItemSpawnerTypeFromID(args[1]) != null) {
                    ItemSpawnerType type = API.getItemSpawnerTypeFromID(args[1]);
                    int amount = 1;
                    for (Player ps : Bukkit.getOnlinePlayers()) {
                        if (API.checkInventory(ps, type.getKendisi().getItemStack())) {
                            ps.getInventory().addItem(type.getKendisi().getItemStack());
                            ps.updateInventory();
                        } else {
                            sender.sendMessage(Styles.Player_Inventory_Is_Full.replaceAll("%player%", ps.getName()));
                            ps.sendMessage(Styles.Inventory_Is_Full);
                        }
                    }
                } else {
                    sender.sendMessage(Styles.Wrong_ID);
                }
            } else if (args.length == 3) {
                if (API.getItemSpawnerTypeFromID(args[1]) != null) {
                    ItemSpawnerType type = API.getItemSpawnerTypeFromID(args[1]);
                    int amount = 1;
                    try {
                        amount = Integer.parseInt(args[2]);
                    } catch (Exception e) {
                        sender.sendMessage(helpmessage.toString());
                        return false;
                    }
                    for (Player ps : Bukkit.getOnlinePlayers()) {
                        if (API.checkInventory(ps, type.getKendisi().getItemStack())) {
                            for (int i = 0; i < amount; i++) {
                                ps.getInventory().addItem(type.getKendisi().getItemStack());
                                ps.updateInventory();
                            }
                            ps.sendMessage(Styles.Spawner_Deal);
                        } else {
                            sender.sendMessage(Styles.Player_Inventory_Is_Full.replaceAll("%player%", ps.getName()));
                            ps.sendMessage(Styles.Inventory_Is_Full);
                        }
                    }
                } else {
                    sender.sendMessage(Styles.Wrong_ID);
                }
            } else {
                sender.sendMessage(helpmessage.toString());
            }
        }
        return false;
    }

    private StringBuilder a() {
        helpmessage = new StringBuilder();
        helpmessage.append(Styles.Plugin_Prefix+"\n");
        helpmessage.append(Styles.Help_Deal);
        return helpmessage;
    }

}
