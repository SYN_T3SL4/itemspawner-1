package me.java9.items.commands.itemsp.spawner;

import me.java9.items.api.Styles;
import me.java9.items.commands.CommandInterface;
import me.java9.items.serialize.ItemSpawnerType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class ItemSpawnerListeleCommand implements CommandInterface {

    private StringBuilder liste = a();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender.isOp()) {
            sender.sendMessage(liste.toString());
        } else {
            sender.sendMessage(Styles.No_Permission_Error);
        }
        return false;
    }

    private StringBuilder a() {
        liste = new StringBuilder();
        liste.append(Styles.Help_Prefix+"\n");
        List<String> listele = new ArrayList<>();
        for (ItemSpawnerType type : Styles.ItemSpawner_Types) {
            listele.add(type.getSpawnerid());
        }
        liste.append(Styles.Spawner_List.replaceAll("%Spawner_List%",listele.toString()));
        return liste;
    }
}
